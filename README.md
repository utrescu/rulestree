# Motor de Regles basat en Expression Tree

La idea és crear un validador de regles genèric que es pugui fer servir amb qualsevol tipus d'objectes.

**Per ara només puc validar constants i propietats dels objectes.**

## Funcionament

Es creen les regles a partir de l'objecte Rule.

    List<Rule> reglesUsuari = new List<Rule>
    {
        new Rule ("Username", ExpressionType.Equal, "xavier", RuleType.Value),

    }

Les regles reben com a paràmetres:

- Camp a comprovar
- Operador Linq
- Valor a comprovar (que pot ser una constant o un altre camp)
- Enumerable que indica si el que volem comprovar és una constant o una propietat

O sigui aquesta regla comprova que el camp **Username** de l'objecte sigui igual a **xavier** (RuleType.Value li diu que ha de comparar amb el valor)

    var regla = new Rule ("Username", ExpressionType.Equal, "xavier", RuleType.Value);

I en canvi aquesta regla (degut a RuleType.property) li diu que compari el camp **Username** amb el contingut del camp **Password**

    var regla2 = new Rule ("Username", ExpressionType.NotEqual, "Password", RuleType.Property);

Un cop es tenen les regles les posem en una llista i les compilem totes juntes (perquè les voldrem avaluar totes de cop):

    List<Rule> reglesUsuari = new List<Rule>();
    reglesUsuari.add(regla);
    reglesUsuari.add(regla2);

    var userRules = Rulenator.Compila<Usuari>(reglesUsuari);

La variable userRules servirà per validar si l'objecte que li passem compleix les regles o no

    usuari = new Usuari
            {
                    UsuariId = 0,
                    Username = "xavier",
                    Password = "ies2010"
            }

    if (Rulenator.Check(userRules, usuari))
    {
        Console.WriteLine($"Usuari {usuari.Username} compleix les regles);
    }
    else {
        Console.WriteLine($"Usuari {usuari.Username} NO compleix les regles);
    }

Un avantatge d'aquest sistema és que es poden posar les regles de validació a la base de dades de forma senzilla ja que la única cosa que cal per generar-les és:

- Nom del camp de referència
- Operacio
- Nom de amb què es compara
- Tipus de regla (constant o propietat)
