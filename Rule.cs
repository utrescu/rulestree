using System;
using System.Linq.Expressions;

namespace RulesTree.Models
{

    /// <summary>
    /// Es fa servir per definir el tipus
    /// de regla
    /// </summary>
    public enum RuleType
    {
        Value,
        Property,
    }

    /// <summary>
    /// Business Rules
    /// </summary>
    public class Rule
    {
        public string ValorQueEsCompara { get; set; }
        public ExpressionType Operador { get; set; }
        public string ValorAComparar { get; set; }

        public RuleType TipusDeRegla { get; set; }

        /// <summary>
        /// Defineix una regla
        /// </summary>
        public Rule(string comparat, ExpressionType operador, string valor, RuleType type)
        {
            ValorQueEsCompara = comparat;
            Operador = operador;
            ValorAComparar = valor;
            TipusDeRegla = type;
        }
    }

}