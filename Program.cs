﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using RulesTree.Models;

namespace RulesTree
{
    class Program
    {
        static void Main(string[] args)
        {

            // Definim les regles a aplicar als usuaris i als grups
            // ---------------------------------------------------------------------
            List<Rule> reglesUsuari = new List<Rule>
            {
                new Rule ("Username", ExpressionType.Equal, "xavier", RuleType.Value),
                new Rule ( "UsuariId", ExpressionType.LessThan, "1", RuleType.Value),
                new Rule ("Password", ExpressionType.NotEqual, "Username", RuleType.Property),
            };

            List<Rule> reglesGrup = new List<Rule>
            {
                new Rule ("Descripcio", ExpressionType.NotEqual, null, RuleType.Value),
            };

            var userRules = Rulenator.Compila<Usuari>(reglesUsuari);
            var grupRules = Rulenator.Compila<Grup>(reglesGrup);


            // Generem dades de prova
            // ---------------------------------------------------------------------
            List<Usuari> usuaris = new List<Usuari>() {

                new Usuari
                {
                    UsuariId = 0,
                    Username = "Pere",
                    Password = "x"
                },
                new Usuari
                {
                    UsuariId = 1,
                    Username = "xavier",
                    Password = "Password"
                },
                new Usuari
                {
                    UsuariId=0,
                    Username = "xavier",
                    Password = "xavier"
                },
                new Usuari
                {
                    UsuariId = 0,
                    Username = "xavier",
                    Password = "ies2010"
                }
            };

            List<Grup> grups = new List<Grup>()
            {
                new Grup
                {
                    GrupId = 0,
                    Descripcio = null,
                    UserId = 1
                },
                new Grup
                {
                    GrupId = 1,
                    Descripcio = "Grup dels bons",
                    UserId = 1
                }
            };

            // Comprovem si es compleixen les regles (si no m'importa el resultat)
            // ---------------------------------------------------------------------

            Console.WriteLine("------------------");
            foreach (var usuari in usuaris)
            {
                Console.WriteLine($"Usuari {usuari.Username}: {Rulenator.Check(userRules, usuari)}");
            }

            Console.WriteLine("------------------");
            foreach (var grup in grups)
            {
                Console.WriteLine($"Grup {grup.Descripcio}: {Rulenator.Check(grupRules, grup)}");
            }

            Console.WriteLine("------------------");

        }
    }
}
