namespace RulesTree.Models
{

    public class Usuari
    {
        public int UsuariId { get; set; }
        public string Username { get; set; }

        public string Password { get; set; }
    }
}