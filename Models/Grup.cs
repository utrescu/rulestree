namespace RulesTree.Models
{

    public class Grup
    {
        public int GrupId { get; set; }
        public string Descripcio { get; set; }

        public int UserId { get; set; }
    }
}