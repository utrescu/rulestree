using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using RulesTree.Models;

namespace RulesTree
{
    public class Rulenator
    {
        /// <summary>
        /// Compila les regles per obtenir expressions Linq executables
        /// </summary>
        public static List<Func<T, bool>> Compila<T>(List<Rule> rules)
        {
            var Rules = new List<Func<T, bool>>();

            rules.ForEach(rule =>
            {
                var genericType = Expression.Parameter(typeof(T));
                var key = MemberExpression.Property(genericType, rule.ValorQueEsCompara);
                var propertyType = typeof(T).GetProperty(rule.ValorQueEsCompara).PropertyType;

                BinaryExpression binaryExpression = null;

                switch (rule.TipusDeRegla)
                {
                    case RuleType.Value:
                        // Comparar contra un valor concret
                        var value = Expression.Constant(Convert.ChangeType(rule.ValorAComparar, propertyType));
                        binaryExpression = Expression.MakeBinary(rule.Operador, key, value);
                        break;
                    case RuleType.Property:
                        // Comparar contra el valor d'una propietat
                        var value2 = MemberExpression.Property(genericType, rule.ValorAComparar);
                        binaryExpression = Expression.MakeBinary(rule.Operador, key, value2);
                        break;
                }

                Rules.Add(Expression.Lambda<Func<T, bool>>(binaryExpression, genericType).Compile());
            });

            return Rules;
        }

        public static bool Check<T>(List<Func<T, bool>> rules, T value)
        {
            var resultat = true;
            foreach (var x in rules)
            {
                resultat = resultat && x(value);
            }
            return resultat;
        }

    }
}
